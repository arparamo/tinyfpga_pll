// Code for switching a LEDs using PLL
// Author**: Angel Rodriguez Paramo (arparamo@essbilbao.org / arparamo@gmail.com)
//
// Code from TinyFPGA templates https://github.com/tinyfpga/TinyFPGA-BX

// look in pins.pcf for all the pin names on the TinyFPGA BX board
module top (
    input CLK,    // 16MHz clock

    output USBPU,  // USB pull-up resistor
    output PIN_20,
    output PIN_18,

);
    // drive USB pull-up resistor to '0' to disable USB
    assign USBPU = 0;

    ////////
    // MAKE A SIMPLE BLINK CIRCUIT (CLK=16 MHZ)
    ////////

    reg [26:0] blink_counter; // keep track of time and location in blink_pattern
    wire [7:0] blink_pattern = 8'b00000001;  // pattern that will be flashed over the LED over time

    always @(posedge CLK) begin // increment the blink_counter every clock
        blink_counter <= blink_counter + 1; //<= for Non-blocking
    end

    // light up the LED according to the pattern
    assign PIN_20 = blink_pattern[ blink_counter[26:24] ]; // For [26:28] Blinking every 8 s (2^27/16MHz), with a duration of 1 s (2^24/16MHz)

    ////////
    // MAKE A PLL BLINK CIRCUIT (CLK=32 MHZ)
    ////////
    wire CLK_PLL;

    pll u_pll( .clock_in(CLK), .clock_out(CLK_PLL), .locked() ); // definition in pll.v
    reg [26:0] blink_counter_pll;

    always @(posedge CLK_PLL) begin
        blink_counter_pll <= blink_counter_pll + 1; //<= for Non-blocking
    end

    assign PIN_18 = blink_pattern[ blink_counter_pll[26:24] ]; 

endmodule

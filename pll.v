/**
 * PLL configuration
 *
 * Parametres taken from icepll -i 16 -o 32
 *
 * Given input frequency:       16.000 MHz
 * Requested output frequency:   32.000 MHz
 * Achieved output frequency:    30.000 MHz
 *
 * The formula for the PLL Freq is:
 *
 * ```math
 * F_{PLLOUT} = \frac{F_{REF}}\cross{DIVF+1}{ 2^{DIVQ} \cross(DIVR+1)   }
 *  ``` 
 */

module pll(
	input  clock_in,
	output clock_out,
	output locked
	);

SB_PLL40_CORE #(
		.FEEDBACK_PATH("SIMPLE"),
		// 32 MHz
		.DIVR(4'b0000),	// DIVR=0 
		.DIVF(7'b0111111),	// DIVF=63
		.DIVQ(3'b101),	// DIVQ=5 FOUT=32MHz
		// 8 MHz
		//.DIVR(4'b0001),		// DIVR=1
		//.DIVF(7'b0111111),	// DIVF=63
		//.DIVQ(3'b110),		// DIVQ=7
		.FILTER_RANGE(3'b001)	// 
	) uut (
		.LOCK(locked),
		.RESETB(1'b1),
		.BYPASS(1'b0),
		.REFERENCECLK(clock_in),
		.PLLOUTCORE(clock_out)
		);

endmodule

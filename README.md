# TinyFPGA tests: blinking LEDs using PLL
**Author**: Angel Rodriguez Paramo (arparamo@essbilbao.org / arparamo@gmail.com)


## Installation

From `https://tinyfpga.com/bx/guide.html`

Install tinyprog:
```
pip install apio==0.4.0 tinyprog
apio install system scons icestorm iverilog
apio drivers --serial-enable
```
Check bootloader
```
tinyprog --update-bootloader
```

Install Atom IDEs and apio packages.

## Execution

Upload with Atom using the APIO toolbar.

The code is:
-  `top.v` verilog code
-  `pll.v` code for the PLL clock
- `pins.pcf` fpga pinning

For doing it manually (install also yosys, arachne-pnr and icepack):
```
yosys -p "synth_ice40 -blif hardware.blif" -q top.v pll.v
arachne-pnr -d 8k -P cm81 -p pins.pcf -o hardware.asc -q hardware.blif
icepack hardware.asc hardware.bin
tinyprog -c /dev/ttyS3 -p hardware.bin # In linux # or COM3 in windows
```
For viewing the FPGA floormap `https://github.com/knielsen/ice40_viewer`

## Description

The code include two tests: blinking leds including an addional clock using PLL.

<figure>
<img src="doc/setup.png" alt="alt text" width="300">
  <figcaption><b>Set-up for tinyFPGA for blinking LEDs.</b></figcaption>
</figure>



First blinking leds are activated from the FGPGA Clock (16 MHz) and using a PLL Clock. The PLL Clock frequency can be adjusted in the pll.v module, for the input paramtres use `icepll -i 12 -i <out_freq>`

The formula for the PLL Freq is:

```
F_{PLLOUT} = \frac{F_{REF}}\cross{DIVF+1}{ 2^{DIVQ} \cross(DIVR+1)   }
```

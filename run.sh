# Remove Previous version
rm hardware.blif
rm hardware.asc
rm hardware.bin

# Build
yosys -p "synth_ice40 -blif hardware.blif" -q top.v pll.v
arachne-pnr -d 8k -P cm81 -p pins.pcf -o hardware.asc -q hardware.blif
icepack hardware.asc hardware.bin

# Upload
#tinyprog --pyserial -c COM4 --program hardware.bin
python -m tinyprog -c /dev/ttyS4 -p hardware.bin # In linux
